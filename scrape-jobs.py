# Job board scraper
# Copyright 2024 Mikael Broadfoot
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.select import Select
from selenium.webdriver.remote.webelement import WebElement

import re

from typing import Callable

from dataclasses import dataclass

options = Options()
options.add_argument("--headless=new")

driver = webdriver.Chrome(options=options)
driver.implicitly_wait(5)

@dataclass
class JobBoard:
    company: str
    url: str
    get_jobs: Callable[[webdriver], list[str]] # https://www.selenium.dev/documentation/webdriver/elements/finders/
    filter_jobs: Callable[[str], bool] = lambda _: True

job_boards = [
        # JobBoard("Company", "https://company.com/jobs", lambda driver: driver.find_element(by=By.CSS_SELECTOR, value="#jobs-table").text.split("\n"), lambda job: re.search('remote', job, re.IGNORECASE)),
        ]

for job_board in job_boards:
    try:
        print('Jobs at {0}:'.format('\x1b]8;;{0}\x1b\\{1}\x1b]8;;\x1b\\'.format(job_board.url, job_board.company)))
        driver.get(job_board.url)
        jobs = job_board.get_jobs(driver)
        for job in jobs:
            if job_board.filter_jobs(job):
                print("\t" + (job.text if type(job) is WebElement else job))
    except:
        pass

driver.quit()
